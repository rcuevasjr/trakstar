Rails.application.routes.draw do
  resources :kudos
  resources :users
      

      post '/give_kudos', to: 'kudos#give_kudos', as: 'give_kudos'
      get '/show_kudos_given', to: 'kudos#show_kudos_given', as: 'show_kudos_given'
      get '/show_kudos_received', to: 'kudos#show_kudos_received', as: 'show_kudos_received'

      post 'authenticate', to: 'authentication#authenticate'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
