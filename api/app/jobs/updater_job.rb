class UpdaterJob < ApplicationJob
    queue_as :default

    def perform(*args)

        time_date =  "Sunday 12:00am"

        UpdaterJob.set(wait_until: Chronic.parse(time_date)).perform_later 'task'
        puts "Kudos will reset for everyone at #{time_date}"


        User.update_kudos


    end

end
