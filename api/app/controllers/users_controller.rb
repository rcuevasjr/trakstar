class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all
    render json: @users.map{|user| { 'Name': user.name, 'Email': user.email }}
  end

end
