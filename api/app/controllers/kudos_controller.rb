class KudosController < ApplicationController
  before_action :set_kudo, only: [:show, :update, :destroy]



  # GET /kudos
  def index
    @kudos = Kudo.all

    render json: @kudos
  end

  def give_kudos

      user_from = User.where(email: params[:current_user_email])[0]
      if user_from.kudos_remaining > 0
          user_from.kudos_remaining -= 1
          user_from.save
          user_to = User.where(email: params[:user_to_email])[0]
          return Kudos.create(user_from_id: user_from.id, user_to_id: user_to.id, message: params[:message] )
      else
          raise error
      end

  end

  # GET /kudos/1
  def show_kudos_given
      # @user = User.eager_load(:kudos_given).first


      render json: @current_user.kudos_given.map {|kudos| {'Message': kudos.message, 'User to': kudos.user_to.name, 'Date': kudos.created_at.strftime("%m/%d/%y").to_s}}
  end

  def show_kudos_received
      # @user = User.eager_load(:kudos_received).first

      render json: @current_user.kudos_received.map {|kudos| {'Message': kudos.message, 'User from': kudos.user_from.name, 'Date': kudos.created_at.strftime("%m/%d/%y").to_s}}
  end


    def kudo_params
      params.require(:kudo).permit(:user_id, :message)
    end
end
