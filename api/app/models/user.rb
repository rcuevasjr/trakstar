class User < ApplicationRecord
  has_secure_password


  has_many :kudos_given, :foreign_key => :user_from_id, :class_name => :Kudos
  has_many :kudos_received, :foreign_key => :user_to_id, :class_name => :Kudos


  validates :email, presence: true, uniqueness: true

  def self.update_kudos
      User.update_all(kudos_remaining: 3)
  end

end
