class CreateKudos < ActiveRecord::Migration[5.2]
  def change
    create_table :kudos do |t|
        t.string :message
        t.timestamps
    end

    add_reference :kudos, :user_from, refereces: :users, index: true
    add_reference :kudos, :user_to, refereces: :users, index: true
    add_foreign_key :kudos, :users, column: :user_from_id
    add_foreign_key :kudos, :users, column: :user_to_id
  end
end
