# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Kudos.destroy_all
User.destroy_all



columns = [:email, :password_digest, :name, :kudos_remaining]
user_data = []

weekly_kudos_range = (1..3).to_a

(1..100).each do |num|
     user_data << [Faker::Internet.email,  BCrypt::Password.create(SecureRandom.uuid), Faker::Movies::Hobbit.character, weekly_kudos_range.sample ]
end

user_data << ['admin@trakstar.com', BCrypt::Password.create('admin'), 'Spencer', 3]

User.import columns, user_data

users = User.pluck :id
columns = [:user_from_id, :user_to_id, :message]
kudos_data = []

 (1..1280).each do |num|
     random_users = Set[]
     while random_users.length < 2
         random_users << users.sample
     end
      payload = random_users.to_a
      payload << Faker::Movies::Hobbit.quote
     kudos_data << payload

 end

 Kudos.import columns, kudos_data
