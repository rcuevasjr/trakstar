import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Me from '@material-ui/icons/AccessibilityNew';
import Group from '@material-ui/icons/Group';
import You from '@material-ui/icons/TouchApp';
import Widget from '@material-ui/icons/Widgets'
import Paper from '@material-ui/core/Paper';


import SideDrawer from './sideDrawer'
import Table from './table'
// import UserTable from './userTable'
import Nav from './nav'

const drawerWidth = 240;
const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});


class PermanentDrawerLeft extends Component {

  constructor(props){
    super(props)
    this.state = {
      content: [],
      user_search: false,
      current_user_name: this.props.userData.name,
      current_user_email: this.props.userData.email



    }

    this.reRender = this.reRender.bind(this)

  }


  async reRender({data, user_search}){


    this.setState({ content: data, user_search })

  }

  render(){



    let screen;

    if (this.state.content.length === 0) {

      let cover = ( <img src={require('./logo.svg')}  alt="Smiley face"  width="100%" />)
      screen = (<Table data={this.state.content} kudos_remaining={localStorage.getItem('kudos_remaining')} current_user_email={this.state.current_user_email} cover={cover} />)

    }else if(this.state.user_search)  {
        screen = (<Table data={this.state.content} kudos_remaining={localStorage.getItem('kudos_remaining')} current_user_email={this.state.current_user_email} user_search={true}  />)

  }else {
      screen = (<Table data={this.state.content} kudos_remaining={localStorage.getItem('kudos_remaining')} current_user_email={this.state.current_user_email}  />)
  }


    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />

        <Nav classes={classes} current_user={this.state.current_user_name} current_user_email={this.state.current_user_email} />

        <Drawer className={classes.drawer} variant="permanent" classes={{ paper: classes.drawerPaper,}} anchor="left">

          <div className={classes.toolbar} />

          <SideDrawer reRender={this.reRender} />

        </Drawer>


        <main className={classes.content}>
          <div className={classes.toolbar} />

            {screen}


        </main>

      </div>
    );
}
}

PermanentDrawerLeft.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PermanentDrawerLeft);
