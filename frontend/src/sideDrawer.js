import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import MenuItem from '@material-ui/core/MenuItem';
import NativeSelect from '@material-ui/core/NativeSelect'
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import Me from '@material-ui/icons/AccessibilityNew';
import Group from '@material-ui/icons/Group';
import You from '@material-ui/icons/TouchApp';
import Widget from '@material-ui/icons/Widgets'
import Table from './table'
import { Link } from "react-router-dom";

import axios from 'axios'





const SideDrawer = (props) => {


    async function handleClick(e, path){
        let authorization = localStorage.getItem('Authorization')
        let config = {
            "headers": {"Authorization": authorization}
        }

        let response;
        let user_search = false

        if (path === 'to_user'){
            response =  await axios.get('/show_kudos_received',config)
        }else if (path === 'from_user'){
            response = await axios.get('/show_kudos_given',config)
        }else{
            response = await axios.get('/users',config)
            user_search = true

        }

        props.reRender({data: response.data, user_search})
    }

    function signOut(e){
        e.preventDefault();
        localStorage.removeItem('Authorization')
        localStorage.removeItem('userData')
        window.location.reload();
    }


  return (

      <div>

        <Divider />
        <List>

          <ListItem button key={'Kudos to me'} value={'to_user'} onClick={(e) => handleClick(e,'to_user')}>
            <ListItemIcon><Me /></ListItemIcon>
            <ListItemText primary={'Kudos to me'} />
          </ListItem>
          <ListItem button key={'Kudos from me'} value={'from_user'} onClick={(e) => handleClick(e, 'from_user')}>
            <ListItemIcon><Group /></ListItemIcon>
            <ListItemText primary={'Kudos from me'} />
          </ListItem>
          <ListItem button key={'peeps'} value={'peeps'} onClick={(e) => handleClick(e,'peeps')}>
            <ListItemIcon><You /></ListItemIcon>
            <ListItemText primary={'Search for Peeps'} />
          </ListItem>

        </List>
        <Divider />
        <List>

        </List>
        <Link to="/"
        style={{ fontSize: '1.5rem', position:'absolute', bottom:'10px'}}
        onClick={(e) => signOut(e)}
         >Sign out</Link>



    </div>
)



}


export default SideDrawer
