import React from 'react';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Main from './main'
import Login from './login'



function App() {
    let key = localStorage.getItem('Authorization')

    let userData =  JSON.parse(localStorage.getItem('userData'))

    let response;
    if (!key){
        response = ( <Route path="/" exact component={Login} />)
    }else{
        response = (<Route path="/" exact component={() => <Main userData={userData} />} />)
    }
  return (
       <Router>
        <div className="App">
            {response}
        </div>
     </Router>
  );
}

export default App;
