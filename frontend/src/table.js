import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import { AutoSizer, Column, SortDirection, Table } from 'react-virtualized';
import AddKudos from  '@material-ui/icons/AddComment'
import axios from 'axios'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';



const styles = theme => ({
  table: {
    fontFamily: theme.typography.fontFamily,
  },
  flexContainer: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  tableRow: {
    cursor: 'pointer',
  },
  tableRowHover: {
    '&:hover': {
      backgroundColor: theme.palette.grey[200],
    },
  },
  tableCell: {
    flex: 1,
  },
  noClick: {
    cursor: 'initial',
  },
});

class MuiVirtualizedTable extends React.PureComponent {




  getRowClassName = ({ index }) => {
    const { classes, rowClassName, onRowClick, button } = this.props;

    return classNames(classes.tableRow, classes.flexContainer, rowClassName, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null,
    });
  };

  cellRenderer = ({ cellData, columnIndex = null }) => {
    const { columns, classes, rowHeight, onRowClick } = this.props;


    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer, {
          [classes.noClick]: onRowClick == null,
        })}
        variant="body"
        style={{ height: rowHeight }}
        align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
      >
        {cellData}
      </TableCell>
    );

    // return <PopUp />

  };

  headerRenderer = ({ label, columnIndex, dataKey, sortBy, sortDirection }) => {
    const { headerHeight, columns, classes, sort } = this.props;
    const direction = {
      [SortDirection.ASC]: 'asc',
      [SortDirection.DESC]: 'desc',
    };

    const inner =
      !columns[columnIndex].disableSort && sort != null ? (
        <TableSortLabel active={dataKey === sortBy} direction={direction[sortDirection]}>
          {label}
        </TableSortLabel>
      ) : (
        label
      );


    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer, classes.noClick)}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? 'right' : 'left'}
      >
        {inner}
      </TableCell>
    );
  };

  render() {
    const { classes, columns, ...tableProps } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            className={classes.table}
            height={height}
            width={width}
            {...tableProps}
            rowClassName={this.getRowClassName}
          >
            {columns.map(({ cellContentRenderer = null, className, dataKey, ...other }, index) => {
              let renderer;
              if (cellContentRenderer != null) {
                renderer = cellRendererProps =>
                  this.cellRenderer({
                    cellData: cellContentRenderer(cellRendererProps),
                    columnIndex: index,
                  });
              } else {
                renderer = this.cellRenderer;
              }

              return (
                <Column
                  key={dataKey}
                  headerRenderer={headerProps =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className={classNames(classes.flexContainer, className)}
                  cellRenderer={renderer}
                  dataKey={dataKey}
                  {...other}
                />
              );
            })}
          </Table>
        )}
      </AutoSizer>
    );
  }
}

MuiVirtualizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      cellContentRenderer: PropTypes.func,
      dataKey: PropTypes.string.isRequired,
      width: PropTypes.number.isRequired,
    }),
  ).isRequired,
  headerHeight: PropTypes.number,
  onRowClick: PropTypes.func,
  rowClassName: PropTypes.string,
  rowHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.func]),
  sort: PropTypes.func,
};

MuiVirtualizedTable.defaultProps = {
  headerHeight: 56,
  rowHeight: 56,
};

const WrappedVirtualizedTable = withStyles(styles)(MuiVirtualizedTable);



class ReactVirtualizedTable extends React.Component {


  constructor(props){
    super(props)

    this.state = {
      open: false,
      email: '',
      name: '',
      value: '',
      kudos_remaining: props.kudos_remaining
  }
  this.handleClickOpen = this.handleClickOpen.bind(this)
  this.handleClose = this.handleClose.bind(this)
  this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
}


handleClose = () => {
    this.setState({ open: false });
  };

 handleClickOpen = (e) => {
     if (this.props.user_search){
          let  { Name, Email } = e.rowData
          this.setState({ open: true, name: Name, email: Email });
      }
};



    handleChange(event) {
        this.setState({value: event.target.value});
      }

    async handleSubmit(event) {
        event.preventDefault();
        let authorization = localStorage.getItem('Authorization')
        let config = {
            "headers": {"Authorization": authorization}
        }


        let response =  await axios.post('/give_kudos',{
            current_user_email: this.props.current_user_email,
            user_to_email: this.state.email,
            message: this.state.value
        }, config)

            let kudos_remaining = this.state.kudos_remaining - 1
            localStorage.setItem('kudos_remaining', kudos_remaining)
            this.setState({ open: false, value: '', kudos_remaining });
             // window.location.reload();


      }


    render(){

      if (this.props.cover){

        return (
          <Paper style={{ height: 600, width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            {this.props.cover}
          </Paper>
        )
      }


      const rows = this.props.data

       if(this.props.user_search){

          for( let i in rows){
              let user = rows[i]
              user['Give Kudos'] = <AddKudos />
          }
      }

      let thing = this.props.data[0]
      let thing2 = []

      for( let i in thing){
        thing2.push(
          {
            width: '120',
            flexGrow: 1,
            label: i,
            dataKey: i
          }
        )
      }

      let modal = (<div></div>)

      if (this.state.kudos_remaining > 0 && this.props.user_search){
          modal = (
              <div>

                <Dialog
                  open={this.state.open}
                  onClose={this.handleClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">{`Give ${this.state.name} Kudos` }</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Personalize your Kudos with a nice custom message !
                    </DialogContentText>
                    <form  onSubmit={this.handleSubmit}>
                        <TextField
                            autoFocus
                            value={this.state.value}
                            onChange={this.handleChange}
                            margin="dense"
                            id="name"
                            label="Kudos"

                            fullWidth
                        />
                        <input type="submit" value="Submit" />
                    </form>
                  </DialogContent>
                  <Button onClick={this.handleClose} color="primary">
                    Close
                  </Button>

                </Dialog>
              </div>
          )
      }




  return (
      <div>
          <p style={{textAling: 'center'}}> Kudos left this week: {this.state.kudos_remaining}</p>
          <Paper style={{ height: 600, width: '100%' }}>
          <WrappedVirtualizedTable
            rowCount={rows.length}
            rowGetter={({ index }) => rows[index]}
            onRowClick={event => this.handleClickOpen(event)}
            columns={thing2}

          />
          {modal}

        </Paper>
      </div>
  );
}
}

export default ReactVirtualizedTable;
