import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloudIcon from '@material-ui/icons/WbCloudy';
import AccountCircle from '@material-ui/icons/AccountCircle'
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from "react-router-dom";

const Nav = (props) => (

  <AppBar style={{backgroundColor: '#faaf50',display: 'flex'}} position="fixed" className={props.classes.appBar}>
    <Toolbar style={{display: 'flex', justifyContent: 'space-between'}}>
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <CloudIcon style={{marginRight: '1em'}} />
        <Typography variant="h6" color="inherit" noWrap>
            Kudos
            </Typography>
    </div>
        <div style={{display: 'flex',alignItems: 'center', flexDirection: 'column', padding: '1em'}}>
            <AccountCircle style={{justifyContent: 'flex-end', marginBottom: '-1rem'}} />
            <p>{props.current_user + ':  ' + props.current_user_email }</p>
            

        </div>

    </Toolbar>
  </AppBar>

)

export default Nav
